package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"

	echo "github.com/labstack/echo/v4"
	"go.uber.org/fx"
)

// NewServer creates and  starts an engine
func NewServer(lc fx.Lifecycle) *echo.Echo {
	e := echo.New()
	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			log.Print("Server is starting up")
			go e.Start(":3001")
			return nil
		},
		OnStop: func(ctx context.Context) error {
			log.Print("Server is shutting down")
			return e.Shutdown(ctx)
		},
	})

	return e
}

func RegisterRoutes(engine *echo.Echo) {
	v1 := engine.Group("api/v1")
	v1.GET("/rates", getRates)
	v1.POST("/rates", postRates)

}

func main() {
	app := fx.New(
		fx.Provide(
			NewServer,
		),
		fx.Invoke(RegisterRoutes),
	)
	app.Run()
}

type currency struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}

type ResultStream struct {
	Result *currency
	Err    error
}

type response map[string]string

func Response(c []*currency) response {
	var r = response{}
	for _, v := range c {
		r[v.Symbol] = v.Price
	}
	return r
}

func ratesHandler(symbols []string) ([]*currency, error) {
	symbolsCount := len(symbols)
	var rates = make([]*currency, 0)
	var ch = make(chan *ResultStream, symbolsCount)
	var wg sync.WaitGroup

	for _, symbol := range symbols {
		wg.Add(1)
		go getPrice(symbol, &wg, ch)
	}
	wg.Wait()
	close(ch)

	for rate := range ch {
		if rate.Err != nil {
			return nil, rate.Err
		}
		rates = append(rates, rate.Result)
	}

	return rates, nil
}

func getRates(c echo.Context) error {
	pairs := c.QueryParam("pairs")
	symbols := strings.Split(pairs, ",")
	rates, err := ratesHandler(symbols)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, Response(rates))
}

func postRates(c echo.Context) error {
	symbols := struct {
		Pairs []string `json:"pairs"`
	}{}
	err := c.Bind(&symbols)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	rates, err := ratesHandler(symbols.Pairs)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, Response(rates))
}

func getPrice(symbol string, w *sync.WaitGroup, ch chan<- *ResultStream) {
	defer w.Done()

	req, err := http.NewRequest("GET", fmt.Sprintf("https://api.binance.com/api/v3/ticker/price?symbol=%s", symbol), nil)
	if err != nil {
		ch <- &ResultStream{
			Result: nil,
			Err:    err,
		}
		return
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		ch <- &ResultStream{
			Result: nil,
			Err:    err,
		}
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		ch <- &ResultStream{
			Result: nil,
			Err:    err,
		}
		return
	}
	defer resp.Body.Close()

	result := &currency{}
	err = json.Unmarshal(body, result)
	if err != nil {
		ch <- &ResultStream{
			Result: nil,
			Err:    err,
		}
		return
	}

	ch <- &ResultStream{
		Result: result,
		Err:    nil,
	}
	return
}
