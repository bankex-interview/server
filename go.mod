module gitlab.com/bankex-interview/server

go 1.14

require (
	github.com/labstack/echo/v4 v4.2.0
	go.uber.org/fx v1.13.1
)
